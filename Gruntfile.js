var concatSrc = [
    'src/js/libs/jquery-2.1.4.min.js', // for now the framework relies on jQuery
    'src/js/libs/*.js', // load all other available libraries
    'src/js/core/define.js', // define the framework
    'src/js/core/modules/*', // load the core modules (router, events, logging, lazyload, parallax, etc.)
    'src/js/core/core.js', // load main framework functionality
    'src/js/modules/*.js', // load all the project modules
    'src/js/core/init.js' // initialize the framework
];

module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Concatenate js
        concat:
        {
            liquid: {
                src: concatSrc,
                dest: 'src/build/js/<%= pkg.name %>.js.liquid'
            },
            local: {
                src: concatSrc,
                dest: 'src/build/js/<%= pkg.name %>.js'
            }
        },

        // minify js
        uglify:
        {
            build:
            {
                src: 'src/build/js/<%= pkg.name %>.js.liquid',
                dest: 'public/assets/<%= pkg.name %>.min.js.liquid'
            },
            shopify:
            {
                src: 'src/build/js/<%= pkg.name %>.js',
                dest: 'public/assets/<%= pkg.name %>.min.js'
            }
        },

        // JSHint
        jshint: {
            modules: ['src/js/modules/**.js']
        },

        // Process scss files
        sass:
        {
            dist:
            {
                options:
                {
                    style: 'expanded'
                },
                files:
                {
                    'src/build/css/<%= pkg.name %>.css': 'src/css/scss/styles.scss',
                    'src/build/css/staging.css': 'src/css/scss/staging.scss'
                }
            }
        },

        // Concatenate css
        cssmin:
        {
            options:
            {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target:
            {
                src: 'src/build/css/<%= pkg.name %>.css',
                dest: 'assets/<%= pkg.name %>.min.css'
            }
        },

        notify: 
        {
            watchCSS: {
                options: {
                    title: 'Sass',
                    message: '<%= pkg.name %> sass done!'
                }
            },
            watchJS: {
                options: {
                    title: 'JS',
                    message: '<%= pkg.name %> js done!'
                }
            },
            build: {
                options: {
                    title: 'Build',
                    message: '<%= pkg.name %> build done!'
                }
            },
            shopify: {
                options: {
                    title: 'Shopify build',
                    message: '<%= pkg.name %> build done!'
                }
            }
        },



        watch:
        {
            js:
            {
                files: ['src/js/libs/*.js','src/js/modules/*.js','src/js/core/modules/*.js','src/js/core/*.js'],
                tasks: ['js','notify:watchJS']
            },
            css:
            {
                files: ['src/css/scss/**'],
                tasks: ['css', 'notify:watchCSS']
            },
            hint:
            {
                files: ['src/js/modules/*.js'],
                tasks: ['jshint']
            }
        }

    });


    // Load
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');

    // Register
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('js', ['concat:local', 'uglify:shopify']);
    grunt.registerTask('css', ['cssmin']);
};