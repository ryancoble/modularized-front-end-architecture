
RC.modules.exampleModule = (function(RC, $) {


	/**
	 * All modules will need to have an initailization module.
	 * This will be used to bind any events, hook into the router, etc
	 */
	function init() {
		console.log('example module initailized.');


		var events = RC.core.eventService;

		events.setType('route');

		events.add('home_route', 'body.home');
		events.addSubscriber('home_route', 'body.home', function(e) {
			//console.log(e);
		});

		//console.log(added);
		//console.log(subadded);


	}

	return {
		init: init
	};


})(RC, jQuery);
