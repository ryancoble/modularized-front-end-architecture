RC.modules.home = (function(RC, $){

	function jqueryEventTest() {
		var Event = RC.core.eventService;

    	var callback = function(e) {
    		console.log(e);
    	};

    	//EventController.setType('jquery');
    	Event.add('click', 'a.button1', 'section');

    	//subscribers
    	Event.addSubscriber('click', 'a.button1', callback);
    	Event.addSubscriber('click', 'a.button1', callback);
    	Event.addSubscriber('click', 'a.button1', callback);
    	Event.addSubscriber('click', 'a.button1', callback);
	}

	function applicationEventTest() {
		var Event = RC.core.eventService;

    	var callback = function(data) {
    		console.log(data);
    	};

    	Event.setType('application');
    	Event.add('cart_additem', 'div.cart');

    	//subscribers
    	Event.addSubscriber('cart_additem', 'div.cart', callback);
    	Event.addSubscriber('cart_additem', 'div.cart', callback);
    	Event.addSubscriber('cart_additem', 'div.cart', callback);

    	Event.trigger('cart_additem', 'div.cart', {
    		test: 'TESTING'
    	});
	}

	function configurationTest() {
        var config = RC.core.configurationService.getInstance();

		config.set('test', 'THIS IS A TEST');
		var testVal = config.get('test');

		console.log(testVal);
	}

	return {
    init: function() {
    	jqueryEventTest();
    	applicationEventTest();
    	configurationTest();
    }
  };
})(RC, jQuery);