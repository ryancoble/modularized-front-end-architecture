RC.modules.NASA = (function(RC, $){

function astronaut() {
	var suit = false;
	
	function privateHasSuit() {
		return suit;
	}
	function privatePutOnSuit() {
		suit = true;

		return suit;
	}

	return {
		hasSuit: privateHasSuit,
		putOnSuit: privatePutOnSuit
	};
}

var fuel = {};
fuel.level = 0;
fuel.levelMax = 100;

fuel.isFull = function() {
	return this.level < this.levelMax ? false : true;
};
fuel.fill = function() {
	this.level = this.levelMax;
};

function countDown() {
	var value = 10;
	var started = false;
	var timer = 0;
	this.value = function() {
		return started ? value : false;
	};
	this.done = function() {
		return !value ? true : false;
	};
	this.execute = function() {
		timer = setInterval(function() {
			value--;
		}, 1000);
	};
	this.start = function() {
		started = true;
	};
	this.reset = function() {
		clearInterval(timer);
		started = false;
		value = 10;
	};
}


function rocket(countDown, astronaut, fuel) {

	function outputTime(countDown) {
		console.log(countDown.value());
	}

	function reset() {
		countDown.reset();

		//document.getElementById('rocket-mask').style.display = 'block';
		//document.getElementById('rocket').style.display = 'none';
	}

	/*
	 * This method will make our rocket take off
	 */
	this.takeoff = function() {
		if(typeof countDown === 'undefined') return false;
		if(typeof astronaut === 'undefined') return false;
		if(typeof fuel === 'undefined') return false;
		
		if(!astronaut.hasSuit()) astronaut.putOnSuit();
		if(!fuel.isFull()) fuel.fill();

		//start the count down
		countDown.start();

		//while the count down is still happening
		while(!countDown.done()) 
		{
			countDown.execute();

			//output the time left before launch
			outputTime(countDown);
		}

		console.log('Launch rocket!');

		document.getElementById('rocket-mask').style.display = 'none';
		document.getElementById('rocket').style.display = 'block';

		reset();

		return true;
	};
}

function init() {

	var rocketObj = new rocket(new countDown(), new astronaut(), fuel);

	return {
		rocket: rocketObj
	};
}

return {
	init: init
};

})(RC, jQuery);