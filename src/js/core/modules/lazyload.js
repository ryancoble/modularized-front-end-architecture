/*
 * Lazy Loader - for lazy loading image resources
 * Author: Matt Simmons
 * Usage: Give the image elements you wish to load a data-src attribute with the url to the image asset.
 * You must give the loader a parent scope of which to load all surrounding images.
 */
RC.core.lazyloader = (function(RC, $){

	/*
	* scope -> class or id to provide context
	* callback -> optional callback
	*/
	function load(scope, callback) {
		$(scope).find('.lazyload').each(function(index){
			$(this).attr('src', $(this).attr('data-src'))
				.on('load', function() {
					if ((index + 1) === $(scope + ' .lazyload').length && typeof callback === 'function') callback();
				});
		});
	}

	return {
		load: load
	};

})(RC, jQuery);