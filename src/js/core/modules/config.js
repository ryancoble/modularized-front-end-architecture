/*
 * Configuration - for storing application configurations
 * Author: Ryan Coble
 * Usage: 
 */
RC.core.configurationService = (function(RC, $){

	var instance;

	function init() {
		var configs = []; //an array of key/value pairs
		//var storageType = 'memory'; //types: cookie, localstorage, sessionstorage, memory

		//load up all the configs
		/*function loadConfigs() {

		}
		function setType(type) {
			storageType = type;

			if(type === 'sessionstorage')
			{
				loadConfigs();
			}
		}*/

		//get an existing config
		function get(key) {
			if(typeof configs[key] === 'undefined') return '';

			return configs[key];
		}

		//set a config
		function set(key, value) {
			configs[key] = value;
		}

		//remove a config
		/*function remove() {

		}*/

		return {
			//setType: setType,
			get: get,
			set: set
		};
	}

	return {

		//get the current instance
		getInstance: function() {
			if( !instance )
			{
				instance = init();
			}

			return instance;
		}
	};

})(RC, jQuery);