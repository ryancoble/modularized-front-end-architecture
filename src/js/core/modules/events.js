/*
 * Event Service - for easy event handling
 * Author: Ryan Coble
 * Usage: There are a few different ways this service can be used. 
 * It will handle most jquery events as well as being able to create application level custom events
 * When adding a new event you give an optional callback, this is used in place of mulitple subscribers.
 * Also when adding a event you may either give an event scope or it will default to the current document.
 * When using an application level custom event, you must trigger said event manually using the trigger method.
 * If you have trouble getting your event's callback working properly try removing the scope.
 */
RC.core.eventService = (function(RC, $){

	var events = [];
	var eventType = 'jquery';

	/*
	 * Notify all subscribers for a given event
	 */
	function notify(eventKey, e) { 

		//console.log(eventKey);

		//console.log(events[eventKey].subscribers);

		for(var i = 0; i < events[eventKey].subscribers.length; i++) {

			var subscriber = events[eventKey].subscribers[i];

			console.log(typeof subscriber.callback);

			if(typeof subscriber.callback === 'function') subscriber.callback(e);
		}
	}


	/*
	 * Set Event Type
	 */
	function setType(type) {
		eventType = type;
	}

	/*
	 * Add Event
	 */
	function addEvent(eventName, selector, callback, scope) {

	 	if(!scope) scope = document;

	 	var eventKey = eventName + '-' + selector;

	 	//for pubsub implementation
	 	events[eventKey] = {
	 		eventType: eventType,
	 		eventName: eventName,
	 		selector: selector,
	 		scope: scope,
	 		subscribers: [],
	 		callback: null
	 	};

	 	if(typeof callback === 'function') {
		 	switch(eventType) {
		 		case 'jquery':
		 			$(scope).on(eventName, selector, callback);
		 		break;
		 		case 'application':
		 			events[eventKey].callback = callback;
		 		break;
		 		case 'route':
		 			if($(selector).length) callback();
		 		break;
		 	}
	 	}
	 	else
	 	{
	 		switch(eventType) {
		 		case 'jquery':
		 			$(scope).on(eventName, selector, function(e){
				 		//console.log(e);
				 		notify(eventKey, e);
				 	});
		 		break;
		 		case 'route':
		 			if($(selector).length) notify(eventKey, true);
		 		break;

		 	}
	 	}
	 }

	/*
	 * Add Subscriber
	 */
	function addSubscriber(eventName, selector, callback) {
		var eventKey = eventName + '-' + selector;

		//console.log(typeof events[eventKey]);

		if(typeof events[eventKey] === 'undefined') return false;

		var subscriber = {
			callback: callback
		};

		events[eventKey].subscribers.push(subscriber);

		return true;
	}

	/*
	 * Trigger Event
	 */
	function trigger(eventName, selector, data) {
		if(!data) data = {};

		var eventKey = eventName + '-' + selector;
		if(typeof events[eventKey] === 'undefined' || eventType !== 'application') return false;

		if(typeof events[eventKey].callback === 'function') 
		{
			events[eventKey].callback(data);
		}
		else if(events[eventKey].subscribers.length)
		{
			notify(eventKey, data);
		}

		return true;
	}

	return {
		setType: setType,
		add: addEvent,
		addSubscriber: addSubscriber,
		trigger: trigger
	};

})(RC, jQuery);