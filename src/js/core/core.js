
/**
 * Module initialization
 */
RC.initializeModules = function() {

	//module initialization failure flag
	var moduleLoadFailure = false;

	//console.log(RC.modules);

	// load all modules' initialization methods
	for(var module in RC.modules) {

		//console.log(RC.modules[module]);

		if(typeof RC.modules[module] === 'object' && typeof RC.modules[module].init === 'function') RC.modules[module].init();
		else moduleLoadFailure = true;
	}

	return !moduleLoadFailure; //return the inverse
}


/**
 * Execution method
 */
//RC.executeModules = function() {

//}


/**
 * Initialize the framework
 * - core functionality
 * - project specific modules
 */
RC.init = function() {

	RC.initializeModules(); //initialize 
}

